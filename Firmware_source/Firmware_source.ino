//Title:    ShinSaeGae Tracker code
//Author:   Lee Jung Hoon, a modified version of the source code by Mark Ireland (www.stratodean.co.uk)
//Notes:    Big thanks goes to Mark Ireland and all the existing UKHAS enthusiasts who helped with providing example code for others to use.

#include <SoftwareSerial.h>
#include <TinyGPS_UBX.h>
#include <SdFat.h>
#include "rtty.h"
#include "gps.h"

//Setup pins
#define GPSRX 2
#define GPSTX 6
#define NTX2 3
#define REDLED 4
#define GREENLED 5
#define TEMP 7
//Character buffer for transmission
#define DATASIZE 256
char data[DATASIZE];
//Character buffer for battery voltage
#define BUFSIZE 8
char battery[BUFSIZE];

//s_id as sentence id to have a unique number for each transmission
uint16_t s_id = 0;
//Select pin for SD card write
const uint8_t chipSelect = SS;
boolean sdWrite = true;

//initialise our classes
RTTY rtty(NTX2, REDLED);
GPS gps(GPSRX, GPSTX, GREENLED);
SdFat sd;


void setup() {
  pinMode(10, OUTPUT);

  //Intialise our hardware serial port to talk to the GPS at 9600 baud. 
  Serial.begin(9600);
  Serial.println(F("ShinSaeGae Tracker, initialising......"));
  //Check how much RAM we have
  Serial.println(freeRam());
  //Initialise GPS
  gps.start();
  //Initialise SD card
////  if (!sd.begin(chipSelect, SPI_HALF_SPEED)){
////    sdWrite = false;
////    Serial.println(F("no write"));
////  }
////  else
////  {
    Serial.println(F("write"));
    //use logEvent subroutine to indicate new transmission with *
////    logEvent("*");
    //call flashLEDs to flash the LEDs if we can write to the SD card
    // flashLEDs();
////  }
  
  Serial.println(F("GPS and SD initialised"));  
}

//*************************************************************************************
//loop()
//*************************************************************************************

void loop() {

  //Get battery voltage
  dtostrf(get_voltage()/1000,0,1, battery);

  Serial.println(freeRam());  

  //Call gps.get_info and, along with the s_id and battery, put it altogether into the string called 'data'
  snprintf(data, DATASIZE, "$$JHLEE,%d,%s, %s", s_id, gps.get_info(), battery);

  Serial.println(data);
  Serial.println(freeRam());
  //write this data string to the SD card if we are using it
  //// if (sdWrite) logEvent(data);
  //send the data over the radio!
  rtty.send(data);
  Serial.println(freeRam());
  //increment the id next time.
  s_id++;
  delay(500);
}

//*************************************************************************************
//helper routines()
//*************************************************************************************

//subroutine to log the sentences to the SD card
void logEvent(const char *msg) {
  // create dir if needed
  sd.mkdir("");
  // create or open a file for append
  ofstream sdlog("SDLOG.TXT", ios::out | ios::app);
  // append a line to the file
  sdlog << msg << endl;
  // check for errors
  if (!sdlog) sd.errorHalt("append failed");
  sdlog.close();
}

//subroutine to give the amount of ram available on the board
int freeRam () {
  extern int __heap_start, *__brkval; 
  int v; 
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}

//Subroutine to give current battery voltage
//Found this from "http://code.google.com/p/tinkerit/wiki/SecretVoltmeter"
float get_voltage() {
  long result;
  // Read 1.1V reference against AVcc
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA,ADSC));
  result = ADCL;
  result |= ADCH<<8;
  result = 1100000L / result; // Back-calculate AVcc in mV
  return (float)result;  
}

//Subroutine to flash the LEDs
//This helps us know if the SD card is working when we turn the tracker on.
void flashLEDs() {
  //Use this to flash LEDs when sd card is available and writeable
  digitalWrite(GREENLED, LOW);
  digitalWrite(REDLED, LOW);  
  delay(50);
  digitalWrite(GREENLED, HIGH);
  digitalWrite(REDLED, HIGH);  
  delay(150);
  digitalWrite(GREENLED, LOW);
  digitalWrite(REDLED, LOW);  
  delay(50);
  digitalWrite(GREENLED, HIGH);
  digitalWrite(REDLED, HIGH);  
  delay(150);
  digitalWrite(GREENLED, LOW);
  digitalWrite(REDLED, LOW);  
}

